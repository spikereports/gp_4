# Spike Report

## Graphic Programming - Outdoor Lighting

### Introduction

This spike aims at demonstrating the difference between the lighting types of static, stationary and moveable.

### Goals.

1. The Spike Report should answer each of the Gap questions

1. A large landscape scene a large amount (100+) of low-poly foliage (use online assets) and an Atmospheric Fog Actor. Use the Third Person Character blueprint. We will create several copies so that we can compare and contrast the different lighting mobilities. The lighting setups for each copy is slightly different:
    1. Static level:
        1. Static Sky Light, Static Directional Light, and a Sky Sphere
        1. Create several sub-levels which will be Lighting Scenarios for Morning, Midday, Afternoon/Evening, and Night, and a way to toggle the active one in the Level Blueprint
    1. Stationary level:
        1. Stationary Sky Light, Stationary Directional Light, and a Sky Sphere
        1. Create several sub-levels which will be Lighting Scenarios for Morning, Midday, Afternoon/Evening, and Night, and a way to toggle the active one in the Level Blueprint
    1. Moveable level:
        1. Movable Sky Light, Movable Directional Light, a Sky Sphere
        1. Have the level blueprint rotate the directional light and update all relevant actors to have the time of day change (~1 day/night cycle per 2 minutes).
        1. Consider moving the fog for different parts of the day (just using a spline or sine wave).
        1. Use Distance Field Ambient Occlusion


### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Bradley
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

1. [Low poly outdoor object](https://www.cgtrader.com/free-3d-models/plant/leaf/tree2-low-poly)

1. Static/ Stationary Level

    1. [Creating Sublevels](https://answers.unrealengine.com/questions/512084/who-can-explain-of-lighting-scenario-feature.html)

1. Moveable Level

    1. [Day/night cycle](https://www.youtube.com/watch?v=fgvsAdJHX1E)

### Tasks Undertaken

#### Static level

1. Create a new third person template level with no starter content
1. Create a new empty world as the lights will be put in and have it in unlit mode.
1. Place a TemplateFloor in the centre of the world and give it a material (of your choice) and add your outdoor Scenery. 
1. Drag in the ThirdPersonCharacter and allow possession for player 0.
1. Follow the 'Creating Sublevels' guide to help create the sub levels needed.
1. Drag the direction light, sky light and BP_SkySphere into the level and set them to static.
    1. In the BP_SkySphere, set the directional light actor to be the Directional Light in current scene.
1. Once the sublevels have been built, open the main level(Persistent level) blueprint and have it load the different levels.
    1. Set the different levels to be from 1-4 and have a load stream level for the level needed and unload stream level for each level that isnt needed.

#### Stationary level

1. Duplicate the persistent level, allowing you not to have to rebuild the level.
1. Follow the same steps from the static level but set the light actors to be stationary.

#### Movable Level

1. Duplicate the persistent level, allowing you not to have to rebuild the level.
1. Add the light actors into the level and set them to be moveable.
1. Open the level blueprint and make the directional and skysphere move on a event tick.
    1. get a reference to BP_skysphere and 'update sun direction.
    1. Add an addactorlocalrotation that references the directional Light
    1. add a make rotator that has a float + float leading the to the pitch input and connect it to the local rotation.
    1. The event tick delta seconds placed in to the first input in the float + float and a new variable called SunSpeed that will determine the speed into the second input.
    1. Go back to the level and play it and watch the sun rotate around.

### What we found out

1. How to create different light scenerios using different sub levels to create different points during the day
1. Creating a overtime day/night cycle using blueprint scripting.
1. The difference between each type of lighting and how they can effect the world.

### Risks/Open Issues

1. When creating the lighting for each sublevel, ensure that the lighting is built in the sublevels scene and not the Persistent Level.
    1. When Building the lighting, only have one level visible (the eye) when building the scenes. 

### Recommendations

1. When using the static or stationary levels and the way it set up. They would best be used for triggering on events or different levels that would have a day or night level.

1. The moveable level would only be necessary if the world required to have a full day/night cycle since it will have to update all shadows, which could lead to some small performance issues.